package com.icaoyi.datakepegawaian.files

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.icaoyi.datakepegawaian.R
import kotlinx.android.synthetic.main.item_jabatan_list.view.*

class ListFilesAdapter: BaseAdapter {
    var filesList : ArrayList<FilesModel>
    var context: Context? = null
    constructor(context: Context, jabatanModel: ArrayList<FilesModel>) : super(){
        this.filesList = jabatanModel
        this.context = context
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val files = this.filesList[position]
        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = inflator.inflate(R.layout.item_jabatan_list, null)

        view.txt_nama_jabatan.text = files.nama_klp_dok
        view.imageView.setImageResource(R.mipmap.ic_document)

        view.tag = files.id_klp_dok
        return view


    }

    override fun getItem(position: Int): Any {
        return filesList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return filesList.size
    }


}