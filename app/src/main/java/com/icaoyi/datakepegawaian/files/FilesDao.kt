package com.icaoyi.datakepegawaian.files

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import io.reactivex.Observable


@Dao
interface FilesDao {
    @Insert(onConflict = REPLACE)
    fun insert(files: FilesModel)

    @Query("UPDATE files_table SET Nama_Klp_Dok =:Nama_Klp_Dok WHERE id_klp_dok = :ID_Klp_Dok")
    fun update(ID_Klp_Dok: String, Nama_Klp_Dok:String)

    @Query("Delete from files_table WHERE id_klp_dok = :ID_Klp_Dok")
    fun delete(ID_Klp_Dok: String)

    @Query("DELETE FROM files_table")
    fun deleteAll()

    @Query("SELECT * from files_table")
    fun getAll(): Observable<List<FilesModel>>
}