package com.icaoyi.datakepegawaian.files

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.OkHttpResponseListener
import com.icaoyi.datakepegawaian.AllDatabase
import com.icaoyi.datakepegawaian.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.update_jabatan_dialog.view.*
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject


class FilesPresenter (val filesView : FilesView, val ctx: Context) {

    private lateinit var  pd : ProgressDialog
    private var filesDatabase:AllDatabase? = null
    val compositeDisposable = CompositeDisposable()
    init{
        pd = ProgressDialog(ctx)
        pd.setMessage("Loading")
        filesDatabase = AllDatabase.getInstance(ctx)
        getData()
    }


    fun showPD(){
        if (pd !=null && !pd.isShowing)pd.show()
    }

    fun dissPD(){
        if (pd !=null && pd.isShowing)pd.dismiss()
    }

    fun getData(){
        showPD()
        AndroidNetworking
            .get("https://api.jasamedika.co.id/public/service/dokumen")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    val data = arrayListOf<FilesModel>()
                    try {
                        if (response!!.getInt("code")==200){
                            deleteAllDb()
                            val jsonArray = response.getJSONArray("data");
                            for(i in 0 until jsonArray.length()){
                                val jsonObject = jsonArray.getJSONObject(i)
                                data.add(
                                    FilesModel(
                                        jsonObject.getString("ID_Klp_Dok"),
                                        jsonObject.getString("Nama_Klp_Dok")
                                    )
                                )
                                insertToDb(
                                    FilesModel(
                                        jsonObject.getString("ID_Klp_Dok"),
                                        jsonObject.getString("Nama_Klp_Dok")
                                    )
                                )
                            }
                            filesView.onSuccesGet(data)
                        } else{
                            filesView.onFailedGet("Gagal")
                        }

                    } catch (e: Exception) {
                        filesView.onFailedGet("Gagal")
                    } finally {
                        dissPD()
                    }
                }

                override fun onError(anError: ANError?) {
                    filesView.onFailedGet("Gagal")
                    dissPD()
                }

            })

    }

    fun delData(id : String){
        showPD()
        AndroidNetworking
            .delete("https://api.jasamedika.co.id/service/dokumen/"+id)
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsOkHttpResponse(object : OkHttpResponseListener {
                override fun onResponse(response: Response?) {
                    if (response!!.code()==200){
                        deleteToDb(id)
                        filesView.onSuccesDel("Sukses")
                    } else {
                        filesView.onFailedDel("Gagal")
                    }
                    dissPD()
                }

                override fun onError(anError: ANError?) {
                    filesView.onFailedDel("Gagal")
                    dissPD()
                }

            })
    }

    fun updtData(ctx: Context, id: String, update: Boolean, data: FilesModel?){
        showPD()
        val mDialogView = LayoutInflater.from(ctx).inflate(R.layout.update_jabatan_dialog, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(ctx)
            .setView(mDialogView)
            .setCancelable(false)
        //show dialog
        val  mAlertDialog = mBuilder.show()

        mDialogView.img.setImageResource(R.mipmap.ic_document)

        if (update && data !=  null){
            mDialogView.edt_nama_jabatan.setText(data.nama_klp_dok)
            mDialogView.edt_id.visibility = View.GONE
        }

        mDialogView.btn_save.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (update){
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("Nama_Klp_Dok", mDialogView.edt_nama_jabatan.text.toString())
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    AndroidNetworking
                        .put("https://api.jasamedika.co.id/service/dokumen/"+id)
                        .addJSONObjectBody(jsonObject)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponse(object : OkHttpResponseListener{
                            override fun onResponse(response: Response?) {
                                if (response!!.code()==200){
                                    updateToDb(id, mDialogView.edt_nama_jabatan.text.toString())
                                    filesView.onSuccessUpdate("Sukses")
                                } else {
                                    filesView.onFailedUpdate("Gagal")
                                }
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                            override fun onError(anError: ANError?) {
                                filesView.onFailedUpdate("Gagal")
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                        })
                } else {
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("ID_Klp_Dok", mDialogView.edt_id.text.toString())
                        jsonObject.put("Nama_Klp_Dok", mDialogView.edt_nama_jabatan.text.toString())
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    AndroidNetworking
                        .post("https://api.jasamedika.co.id/service/dokumen")
                        .addJSONObjectBody(jsonObject)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponse(object : OkHttpResponseListener{
                            override fun onResponse(response: Response?) {
                                if (response!!.code()==200){
                                    insertToDb(
                                        FilesModel(
                                            mDialogView.edt_id.text.toString(),
                                            mDialogView.edt_nama_jabatan.text.toString()
                                        )
                                    )
                                    filesView.onSuccessCreate("Sukses")
                                } else {
                                    filesView.onFailedCreate("Gagal")
                                }
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                            override fun onError(anError: ANError?) {
                                filesView.onFailedCreate("Gagal")
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                        })
                }
            }

        })

        mDialogView.btn_cancel.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                mAlertDialog.dismiss()
                dissPD()
            }

        })
    }

    fun insertToDb(files: FilesModel){
        compositeDisposable.add(Observable.fromCallable{filesDatabase?.filesDao()?.insert(files)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun updateToDb(ID_Klp_Dok: String, Nama_Klp_Dok:String){
        compositeDisposable.add(Observable.fromCallable{filesDatabase?.filesDao()?.update(ID_Klp_Dok, Nama_Klp_Dok)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun deleteToDb(ID_Klp_Dok: String){
        compositeDisposable.add(Observable.fromCallable{filesDatabase?.filesDao()?.delete(ID_Klp_Dok)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun deleteAllDb(){
        compositeDisposable.add(Observable.fromCallable{filesDatabase?.filesDao()?.deleteAll()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun getAllData(){
        compositeDisposable.add(filesDatabase!!.filesDao().getAll()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                filesView.onSuccesGet(it)
            })
    }

    fun onDestroy() {
        AllDatabase.destroyInstance()
        compositeDisposable.dispose()
    }
}