package com.icaoyi.datakepegawaian.files

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


//Annotation Parcelize for make the data class parcelable
@Parcelize

@Entity(tableName = "files_table")
data class FilesModel(@ColumnInfo(name = "ID_Klp_Dok") var id_klp_dok : String,
                 @ColumnInfo(name = "Nama_Klp_Dok") var nama_klp_dok : String,
                 @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long = 0) :
    Parcelable
