package com.icaoyi.datakepegawaian

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.icaoyi.datakepegawaian.files.FilesDao
import com.icaoyi.datakepegawaian.jabatan.JabatanDao
import com.icaoyi.datakepegawaian.files.FilesModel
import com.icaoyi.datakepegawaian.jabatan.JabatanModel
import com.icaoyi.datakepegawaian.pegawai.PegawaiDao
import com.icaoyi.datakepegawaian.pegawai.PegawaiModel

@Database(entities = arrayOf(JabatanModel::class, FilesModel::class, PegawaiModel::class), version = 2)
abstract class AllDatabase : RoomDatabase() {
    abstract fun jabatanDao() : JabatanDao
    abstract fun filesDao() : FilesDao
    abstract fun pegawaiDao() : PegawaiDao
    companion object {
        private var INSTANCE: AllDatabase? = null

        fun getInstance(context: Context): AllDatabase? {
            if (INSTANCE == null) {
                synchronized(AllDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        AllDatabase::class.java, "pegawaidata.db")
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}