package com.icaoyi.datakepegawaian.jabatan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.icaoyi.datakepegawaian.R
import kotlinx.android.synthetic.main.item_jabatan_list.view.*

class ListJabatanAdapter : BaseAdapter {
    var jabatanList : ArrayList<JabatanModel>
    var context: Context ? = null
    constructor(context: Context, jabatanModel: ArrayList<JabatanModel>) : super(){
        this.jabatanList = jabatanModel
        this.context = context
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val jabatan = this.jabatanList[position]
        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = inflator.inflate(R.layout.item_jabatan_list, null)

        view.txt_nama_jabatan.text = jabatan.nama_Jabatan
        view.imageView.setImageResource(R.mipmap.ic_career)

        view.tag = jabatan.id_Jabatan
        return view


    }

    override fun getItem(position: Int): Any {
        return jabatanList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return jabatanList.size
    }


}