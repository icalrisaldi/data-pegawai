package com.icaoyi.datakepegawaian.jabatan

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

//Annotation Parcelize for make the data class parcelable
@Parcelize

@Entity(tableName = "jabatan_table")
data class JabatanModel(@ColumnInfo(name = "ID_Jabatan") var id_Jabatan : String,
                        @ColumnInfo(name = "Nama_Jabatan") var nama_Jabatan : String,
                        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long = 0) :
    Parcelable {
    override fun toString(): String {
        return nama_Jabatan
    }
}
