package com.icaoyi.datakepegawaian.jabatan

interface JabatanView {
    //getData
    fun onSuccesGet(data : List<JabatanModel>?)
    fun onFailedGet(msg : String)

    //deleteData
    fun onSuccesDel(msg : String)
    fun onFailedDel(msg : String)

    //updateData
    fun onSuccessUpdate(msg : String)
    fun onFailedUpdate(msg : String)

    //createData
    fun onSuccessCreate(msg : String)
    fun onFailedCreate(msg : String)
}