package com.icaoyi.datakepegawaian.jabatan

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.OkHttpResponseListener
import com.icaoyi.datakepegawaian.AllDatabase
import com.icaoyi.datakepegawaian.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.update_jabatan_dialog.view.*
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject


class JabatanPresenter (val jabatanView : JabatanView, val ctx: Context){

    private lateinit var  pd : ProgressDialog
    private var allDatabase: AllDatabase? = null
    val compositeDisposable = CompositeDisposable()

    init{
        pd = ProgressDialog(ctx)
        pd.setMessage("Loading")
        allDatabase = AllDatabase.getInstance(ctx)
        getData()
        /*loadJabatan()*/
    }


    fun showPD(){
        if (pd !=null && !pd.isShowing)pd.show()
    }

    fun dissPD(){
        if (pd !=null && pd.isShowing)pd.dismiss()
    }

    fun getData(){
        showPD()
        AndroidNetworking
            .get("https://api.jasamedika.co.id/service/jabatan")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    deleteAllDb()
                    val data = arrayListOf<JabatanModel>()
                    try {
                        if (response!!.getInt("code")==200){
                            val jsonArray = response.getJSONArray("data");
                            for(i in 0 until jsonArray.length()){
                                val jsonObject = jsonArray.getJSONObject(i)
                                data.add(
                                    JabatanModel(
                                        jsonObject.getString("ID"),
                                        jsonObject.getString("Nama_Jabatan")
                                    )
                                )
                                insertToDb(
                                    JabatanModel(
                                        jsonObject.getString("ID"),
                                        jsonObject.getString("Nama_Jabatan")
                                    )
                                )
                            }
                            jabatanView.onSuccesGet(data)
                        } else{
                            jabatanView.onFailedGet("Gagal")
                        }

                    } catch (e: Exception) {
                        jabatanView.onFailedGet("Gagal")
                    } finally {
                        dissPD()
                    }
                }

                override fun onError(anError: ANError?) {
                    jabatanView.onFailedGet("Gagal")
                    dissPD()
                }

            })

    }

    fun delData(id : String){
        showPD()
        AndroidNetworking
            .delete("https://api.jasamedika.co.id/service/jabatan/"+id)
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsOkHttpResponse(object : OkHttpResponseListener{
                override fun onResponse(response: Response?) {
                    if (response!!.code()==200){
                        delData(id)
                        jabatanView.onSuccesDel("Sukses")
                    } else {
                        jabatanView.onFailedDel("Gagal")
                    }
                    dissPD()
                }

                override fun onError(anError: ANError?) {
                    jabatanView.onFailedDel("Gagal")
                    dissPD()
                }

            })
    }

    fun updtData(ctx: Context, id: String, update: Boolean, data: JabatanModel?){
        showPD()
        val mDialogView = LayoutInflater.from(ctx).inflate(R.layout.update_jabatan_dialog, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(ctx)
            .setView(mDialogView)
            .setCancelable(false)
        //show dialog
        val  mAlertDialog = mBuilder.show()

        mDialogView.img.setImageResource(R.mipmap.ic_career)

        if (update && data !=  null){
            mDialogView.edt_nama_jabatan.setText(data.nama_Jabatan)
            mDialogView.edt_id.visibility = View.GONE
        }

        mDialogView.btn_save.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (update){
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("Nama_Jabatan", mDialogView.edt_nama_jabatan.text.toString())
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    AndroidNetworking
                        .put("https://api.jasamedika.co.id/service/jabatan/"+id)
                        .addJSONObjectBody(jsonObject)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponse(object : OkHttpResponseListener{
                            override fun onResponse(response: Response?) {
                                if (response!!.code()==200){
                                    updateToDb(id,mDialogView.edt_nama_jabatan.text.toString())
                                    jabatanView.onSuccessUpdate("Sukses")
                                } else {
                                    jabatanView.onFailedUpdate("Gagal")
                                }
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                            override fun onError(anError: ANError?) {
                                jabatanView.onFailedUpdate("Gagal")
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                        })
                } else {
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("ID", mDialogView.edt_id.text)
                        jsonObject.put("Nama_Jabatan", mDialogView.edt_nama_jabatan.text)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    AndroidNetworking
                        .post("https://api.jasamedika.co.id/service/jabatan/")
                        .addJSONObjectBody(jsonObject)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponse(object : OkHttpResponseListener{
                            override fun onResponse(response: Response?) {
                                if (response!!.code()==200){
                                    insertToDb(
                                        JabatanModel(
                                            mDialogView.edt_id.text.toString(),
                                            mDialogView.edt_nama_jabatan.text.toString()
                                        )
                                    )
                                    jabatanView.onSuccessCreate("Sukses")
                                } else {
                                    jabatanView.onFailedCreate("Gagal")
                                }
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                            override fun onError(anError: ANError?) {
                                jabatanView.onFailedCreate("Gagal")
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                        })
                }
            }

        })

        mDialogView.btn_cancel.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                mAlertDialog.dismiss()
                dissPD()
            }

        })
    }

    fun insertToDb(jabatan: JabatanModel){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.jabatanDao()?.insert(jabatan)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun updateToDb(ID_Klp_Dok: String, Nama_Klp_Dok:String){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.jabatanDao()?.update(ID_Klp_Dok, Nama_Klp_Dok)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun deleteToDb(ID_Klp_Dok: String){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.jabatanDao()?.delete(ID_Klp_Dok)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun deleteAllDb(){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.jabatanDao()?.deleteAll()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun getAllData(){
        compositeDisposable.add(allDatabase!!.jabatanDao().getAll()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                jabatanView.onSuccesGet(it)
            })
    }

    fun onDestroy() {
        AllDatabase.destroyInstance()
        compositeDisposable.dispose()
    }
}