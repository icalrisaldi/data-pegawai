package com.icaoyi.datakepegawaian.jabatan

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Observable

@Dao
interface JabatanDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(jabatan: JabatanModel)

    @Query("UPDATE jabatan_table SET Nama_Jabatan =:Nama_Jabatan WHERE id_jabatan = :ID_Jabatan")
    fun update(ID_Jabatan: String, Nama_Jabatan:String)

    @Query("Delete from jabatan_table WHERE id_jabatan = :ID_Klp_Dok")
    fun delete(ID_Klp_Dok: String)

    @Query("DELETE FROM jabatan_table")
    fun deleteAll()

    @Query("SELECT * from jabatan_table")
    fun getAll(): Observable<List<JabatanModel>>
}