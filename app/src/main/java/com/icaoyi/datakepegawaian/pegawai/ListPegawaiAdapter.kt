package com.icaoyi.datakepegawaian.pegawai

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.icaoyi.datakepegawaian.R
import kotlinx.android.synthetic.main.item_pegawai_list.view.*

class ListPegawaiAdapter : BaseAdapter {
    var pegawaiList: ArrayList<PegawaiModelLengkap>
    var context: Context? = null

    constructor(context: Context, pegawaiModel: ArrayList<PegawaiModelLengkap>) : super() {
        this.pegawaiList = pegawaiModel
        this.context = context
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val pegawai = this.pegawaiList[position]
        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = inflator.inflate(R.layout.item_pegawai_list, null)

        view.txt_nama_jabatan.text = pegawai.Nama_pegawai
        view.txt_nama_jabatan2.text = pegawai.Nama_Jabatan
        view.txt_kelamin.text = pegawai.Jenis_kelamin_pegawai
        view.txt_ttl.text = pegawai.Tgl_lahir_pegawai
        view.txt_salary.text = pegawai.salary_pegawai

        view.tag = pegawai.ID_pegawai
        return view


    }

    override fun getItem(position: Int): Any {
        return pegawaiList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return pegawaiList.size
    }
}