package com.icaoyi.datakepegawaian.pegawai

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.icaoyi.datakepegawaian.R
import kotlinx.android.synthetic.main.fragment_pegawai.view.*

class PegawaiFragment : Fragment(), PegawaiView {
    private lateinit var presenter: PegawaiPresenter
    var adapter: ListPegawaiAdapter? = null
    var pegawaiModel = ArrayList<PegawaiModelLengkap>()
    lateinit var ctx : Context
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_pegawai, container, false)
        setHasOptionsMenu(true)
        presenter = PegawaiPresenter(this, view.context)
        ctx = view.context

        (activity as AppCompatActivity).supportActionBar?.title = "Pegawai"


        adapter = ListPegawaiAdapter(view.context, pegawaiModel)
        view.g_list_item.adapter = adapter
        view.g_list_item.setOnItemLongClickListener(object : AdapterView.OnItemLongClickListener{
            override fun onItemLongClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ): Boolean {
                //AlertDialogBuilder
                AlertDialog.Builder(ctx)
                    .setCancelable(false)
                    .setTitle("Warning")
                    .setMessage("Are you sure you want to delete this data?")
                    .setNegativeButton("Cancel", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            dialog!!.dismiss()
                        }

                    })
                    .setPositiveButton("Yes", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            dialog!!.dismiss()
                            presenter.delData(view!!.tag.toString())
                        }

                    })
                    .show()
                return true
            }

        })

        view.g_list_item.setOnItemClickListener(object : AdapterView.OnItemClickListener{
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                presenter.updtData(ctx, view!!.tag.toString(), true, pegawaiModel[position])
            }

        })
        //presenter.getData()

        return view
    }

    override fun onSuccesGet(data: List<PegawaiModel>?) {

    }

    override fun onFailedGet(msg: String) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccesGetLengkap(data: List<PegawaiModelLengkap>?) {
        if (data != null) {
            pegawaiModel.clear()
            pegawaiModel.addAll(data)
            adapter?.notifyDataSetChanged()
        }
    }

    override fun onFailedGetLengkap(msg: String) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccesDel(msg: String) {
        presenter.getAllDataLengkap()
    }

    override fun onFailedDel(msg: String) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessUpdate(msg: String) {
        presenter.getAllDataLengkap()
    }

    override fun onFailedUpdate(msg: String) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessCreate(msg: String) {
        presenter.getAllDataLengkap()
    }

    override fun onFailedCreate(msg: String) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.tambah -> {
                presenter.updtData(ctx, "", false, null)
            }
        }
        return false
    }
}