package com.icaoyi.datakepegawaian.pegawai

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.LinearLayout
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.OkHttpResponseListener
import com.icaoyi.datakepegawaian.AllDatabase
import com.icaoyi.datakepegawaian.files.FilesModel
import com.icaoyi.datakepegawaian.jabatan.JabatanModel
import com.icaoyi.datakepegawaian.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.update_pegawai_dialog.view.*
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class PegawaiPresenter (val pegawaiView : PegawaiView, val ctx: Context){
    private lateinit var  pd : ProgressDialog
    private var allDatabase: AllDatabase? = null
    val compositeDisposable = CompositeDisposable()
    var itemSpinnerJabatan = ArrayList<JabatanModel>()
    var itemSpinnerFiles = ArrayList<FilesModel>()

    init{
        pd = ProgressDialog(ctx)
        pd.setMessage("Loading")
        allDatabase = AllDatabase.getInstance(ctx)
        getData()
        /*loadJabatan()*/
    }

    fun showPD(){
        if (pd !=null && !pd.isShowing)pd.show()
    }

    fun dissPD(){
        if (pd !=null && pd.isShowing)pd.dismiss()
    }

    fun getData(){
        showPD()
        AndroidNetworking
            .get("https://api.jasamedika.co.id/service/pegawai")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    deleteAllDb()
                    val data = arrayListOf<PegawaiModel>()
                    try {
                        if (response!!.getInt("code")==200){
                            val jsonArray = response.getJSONArray("data");
                            for(i in 0 until jsonArray.length()){
                                val jsonObject = jsonArray.getJSONObject(i)
                                data.add(
                                    PegawaiModel(
                                        jsonObject.getString("ID"),
                                        jsonObject.getString("Nama"),
                                        jsonObject.getString("Jenis_Kelamin"),
                                        jsonObject.getString("Tgl_Lahir"),
                                        jsonObject.getString("ID_Jabatan"),
                                        jsonObject.getString("Salary"),
                                        jsonObject.getString("Profile_Picture")
                                    )
                                )
                                insertToDb(
                                    PegawaiModel(
                                        jsonObject.getString("ID"),
                                        jsonObject.getString("Nama"),
                                        jsonObject.getString("Jenis_Kelamin"),
                                        jsonObject.getString("Tgl_Lahir"),
                                        jsonObject.getString("ID_Jabatan"),
                                        jsonObject.getString("Salary"),
                                        jsonObject.getString("Profile_Picture")
                                    )
                                )
                            }
                            getDataJabatan()
                        } else{
                            pegawaiView.onFailedGet("Gagal")
                        }

                    } catch (e: Exception) {
                        pegawaiView.onFailedGet("Gagal")
                    } finally {
                        dissPD()
                    }
                }

                override fun onError(anError: ANError?) {
                    pegawaiView.onFailedGet("Gagal")
                    dissPD()
                }

            })

    }

    fun delData(id : String){
        showPD()
        AndroidNetworking
            .delete("https://api.jasamedika.co.id/service/pegawai/"+id)
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsOkHttpResponse(object : OkHttpResponseListener {
                override fun onResponse(response: Response?) {
                    if (response!!.code()==200){
                        delData(id)
                        pegawaiView.onSuccesDel("Sukses")
                    } else {
                        pegawaiView.onFailedDel("Gagal")
                    }
                    dissPD()
                }

                override fun onError(anError: ANError?) {
                    pegawaiView.onFailedDel("Gagal")
                    dissPD()
                }

            })
    }

    fun updtData(ctx: Context, id: String, update: Boolean, data: PegawaiModelLengkap?){
        showPD()
        val mDialogView = LayoutInflater.from(ctx).inflate(R.layout.update_pegawai_dialog, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(ctx)
            .setView(mDialogView)
            .setCancelable(false)
        //show dialog
        val  mAlertDialog = mBuilder.show()

        var spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, itemSpinnerJabatan);
        mDialogView.spin_jabatan.adapter = spinnerAdapter

        mDialogView.lin_files

        if (update && data !=  null){
            mDialogView.edt_nama_pegawai.setText(data.Nama_pegawai)
            if(data.Jenis_kelamin_pegawai.toLowerCase().equals("laki-laki")){
                mDialogView.l.isChecked = true
            } else{
                mDialogView.p.isChecked = true
            }
            var position = 0
            for(i in 0 until itemSpinnerJabatan.size){
                if (itemSpinnerJabatan[i].id_Jabatan==data.ID_jabatan){
                    position = i
                }
            }

            mDialogView.spin_jabatan.setSelection(position)

            mDialogView.edt_ttl.setText(data.Tgl_lahir_pegawai)
            mDialogView.edt_salary.setText(data.salary_pegawai)
            mDialogView.edt_nama_pegawai.setText(data.Nama_pegawai)
            mDialogView.edt_id.visibility = View.GONE
        } else{
            for(i in 0 until itemSpinnerFiles.size){
                val chk = CheckBox(mDialogView.context)
                chk.setText(itemSpinnerFiles[i].nama_klp_dok)
                chk.setTag(itemSpinnerFiles[i].id_klp_dok)
                mDialogView.lin_files.addView(chk)
            }
        }

        mDialogView.btn_save.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (update){
                    val jsonObject = JSONObject()
                    try {
                        jsonObject.put("ID", id)
                        jsonObject.put("Nama", mDialogView.edt_nama_pegawai.text.toString())
                        jsonObject.put("Jenis_Kelamin", if(mDialogView.l.isChecked)"Laki-Laki" else "Perempuan")
                        jsonObject.put("Tgl_Lahir", mDialogView.edt_ttl.text.toString())
                        jsonObject.put("ID_Jabatan", itemSpinnerJabatan[mDialogView.spin_jabatan.selectedItemPosition].id_Jabatan)
                        jsonObject.put("Salary", mDialogView.edt_salary.text.toString())
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    AndroidNetworking
                        .put("https://api.jasamedika.co.id/service/pegawai/"+id)
                        .addJSONObjectBody(jsonObject)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponse(object : OkHttpResponseListener{
                            override fun onResponse(response: Response?) {
                                if (response!!.code()==200){
                                    updateToDb(id,mDialogView.edt_nama_pegawai.text.toString(),if(mDialogView.l.isChecked)"Laki-Laki" else "Perempuan",mDialogView.edt_ttl.text.toString(),itemSpinnerJabatan[mDialogView.spin_jabatan.selectedItemPosition].id_Jabatan,mDialogView.edt_salary.text.toString())
                                    pegawaiView.onSuccessUpdate("Sukses")
                                } else {
                                    pegawaiView.onFailedUpdate("Gagal")
                                }
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                            override fun onError(anError: ANError?) {
                                pegawaiView.onFailedUpdate("Gagal")
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                        })
                } else {
                    val jsonObject = JSONObject()

                    try {


                        try {
                            val jsonArray = JSONArray()

                            val layout: LinearLayout = mDialogView.lin_files
                            val count = layout.childCount

                            for (i in 0 until count) {

                                var v : View = layout.getChildAt(i)
                                if (v is CheckBox) {
                                    if (v.isChecked){
                                        val jsonObject2 = JSONObject()
                                        jsonObject2.put("ID", v.tag.toString())
                                        jsonObject2.put("ID_Klp_Dok", v.text.toString())
                                        jsonArray.put(jsonObject2)
                                    }
                                }

                            }

                            jsonObject.put("ID", mDialogView.edt_id.text.toString())
                            jsonObject.put("Nama", mDialogView.edt_nama_pegawai.text.toString())
                            jsonObject.put("Jenis_Kelamin", if(mDialogView.l.isChecked)"Laki-Laki" else "Perempuan")
                            jsonObject.put("Tgl_Lahir", mDialogView.edt_ttl.text.toString())
                            jsonObject.put("ID_Jabatan", itemSpinnerJabatan[mDialogView.spin_jabatan.selectedItemPosition].id_Jabatan)
                            jsonObject.put("Salary", mDialogView.edt_salary.text.toString())
                            jsonObject.put("Profile_Picture", "120003.png")
                            jsonObject.put("documents", jsonArray)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    AndroidNetworking
                        .post("https://api.jasamedika.co.id/service/jabatan/")
                        .addJSONObjectBody(jsonObject)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponse(object : OkHttpResponseListener{
                            override fun onResponse(response: Response?) {
                                if (response!!.code()==200){
                                    insertToDb(
                                        PegawaiModel(
                                            mDialogView.edt_id.text.toString(),
                                            mDialogView.edt_nama_pegawai.text.toString(),
                                            if(mDialogView.l.isChecked)"Laki-Laki" else "Perempuan",
                                            mDialogView.edt_ttl.text.toString(),
                                            itemSpinnerJabatan[mDialogView.spin_jabatan.selectedItemPosition].id_Jabatan,
                                            mDialogView.edt_salary.text.toString(),
                                            "120003.png"
                                        )
                                    )
                                    pegawaiView.onSuccessCreate("Sukses")
                                } else {
                                    pegawaiView.onFailedCreate("Gagal")
                                }
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                            override fun onError(anError: ANError?) {
                                pegawaiView.onFailedCreate("Gagal")
                                dissPD()
                                mAlertDialog.dismiss()
                            }

                        })
                }
            }

        })

        mDialogView.btn_cancel.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                mAlertDialog.dismiss()
                dissPD()
            }

        })
    }

    fun insertToDb(pegawai: PegawaiModel){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.pegawaiDao()?.insert(pegawai)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun updateToDb(ID_pegawai: String,
                   Nama_pegawai:String,
                   jenis_kelamin_pegawai:String,
                   tgl_lahir_pegawai:String,
                   id_jabatan:String,
                   salary_pegawai:String){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.pegawaiDao()?.update(ID_pegawai,
            Nama_pegawai,
            jenis_kelamin_pegawai,
            tgl_lahir_pegawai,
            id_jabatan,
            salary_pegawai)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun deleteToDb(ID_Klp_Dok: String){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.pegawaiDao()?.delete(ID_Klp_Dok)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun deleteAllDb(){
        compositeDisposable.add(Observable.fromCallable{allDatabase?.pegawaiDao()?.deleteAll()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
    }

    fun getAllData(){
        compositeDisposable.add(allDatabase!!.pegawaiDao().getAll()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                pegawaiView.onSuccesGet(it)
            })
    }

    fun getAllDataLengkap(){
        compositeDisposable.add(allDatabase!!.pegawaiDao().getAllTampilanPegawai()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{

                pegawaiView.onSuccesGetLengkap(it)
            })
    }

    fun getDataJabatan(){
        compositeDisposable.add(allDatabase!!.pegawaiDao().getJabatan()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                itemSpinnerJabatan.clear()
                itemSpinnerJabatan.addAll(it)
                getDataFiles()
            })
    }

    fun getDataFiles(){
        compositeDisposable.add(allDatabase!!.pegawaiDao().getAllFile()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                itemSpinnerFiles.clear()
                itemSpinnerFiles.addAll(it)
                getAllDataLengkap()
            })
    }

    fun onDestroy() {
        AllDatabase.destroyInstance()
        compositeDisposable.dispose()
    }
}