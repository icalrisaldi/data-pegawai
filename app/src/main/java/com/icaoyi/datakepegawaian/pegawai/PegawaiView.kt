package com.icaoyi.datakepegawaian.pegawai


interface PegawaiView {
    //getData
    fun onSuccesGet(data : List<PegawaiModel>?)
    fun onFailedGet(msg : String)

    //getData
    fun onSuccesGetLengkap(data : List<PegawaiModelLengkap>?)
    fun onFailedGetLengkap(msg : String)

    //deleteData
    fun onSuccesDel(msg : String)
    fun onFailedDel(msg : String)

    //updateData
    fun onSuccessUpdate(msg : String)
    fun onFailedUpdate(msg : String)

    //createData
    fun onSuccessCreate(msg : String)
    fun onFailedCreate(msg : String)
}