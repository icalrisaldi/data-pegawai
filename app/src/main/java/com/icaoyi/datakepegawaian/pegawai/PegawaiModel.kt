package com.icaoyi.datakepegawaian.pegawai

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize

@Entity(tableName = "pegawai_table")
data class PegawaiModel( @ColumnInfo(name = "ID_pegawai") var id_pegawai : String,
                         @ColumnInfo(name = "Nama_pegawai") var nama_pegawai : String,
                         @ColumnInfo(name = "Jenis_kelamin_pegawai") var jenis_kelamin_pegawai : String,
                         @ColumnInfo(name = "Tgl_lahir_pegawai") var tgl_lahir_pegawai : String,
                         @ColumnInfo(name = "ID_jabatan") var id_jabatan : String,
                         @ColumnInfo(name = "salary_pegawai") var salary_pegawai : String,
                         @ColumnInfo(name = "foto_profil_pegawai") var foto_profile_pegawai : String,
                         @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long = 0) :
    Parcelable