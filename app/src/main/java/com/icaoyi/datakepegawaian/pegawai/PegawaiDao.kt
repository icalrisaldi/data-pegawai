package com.icaoyi.datakepegawaian.pegawai

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.icaoyi.datakepegawaian.files.FilesModel
import com.icaoyi.datakepegawaian.jabatan.JabatanModel
import io.reactivex.Observable

@Dao
interface PegawaiDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pegawai: PegawaiModel)

    @Query("UPDATE pegawai_table SET Nama_pegawai =:Nama_pegawai, " +
            "                              jenis_kelamin_pegawai =:jenis_kelamin_pegawai," +
            "                              tgl_lahir_pegawai =:tgl_lahir_pegawai," +
            "                              id_jabatan =:id_jabatan," +
            "                              salary_pegawai =:salary_pegawai " +
            "WHERE id_pegawai = :ID_pegawai")
    fun update(ID_pegawai: String,
               Nama_pegawai:String,
               jenis_kelamin_pegawai:String,
               tgl_lahir_pegawai:String,
               id_jabatan:String,
               salary_pegawai:String)

    @Query("Delete from pegawai_table WHERE id_jabatan = :ID_pegawai")
    fun delete(ID_pegawai: String)

    @Query("DELETE FROM pegawai_table")
    fun deleteAll()

    @Query("SELECT * from pegawai_table")
    fun getAll(): Observable<List<PegawaiModel>>

    @Query("Select * from jabatan_table")
    fun getJabatan() : Observable<List<JabatanModel>>

    @Query("Select * from jabatan_table where id_jabatan = :id_jabatan ")
    fun getJabatanbyPegawai(id_jabatan: String) : Observable<List<JabatanModel>>

    @Query("SELECT * from files_table")
    fun getAllFile(): Observable<List<FilesModel>>

    @Query("SELECT IFNULL(j.nama_jabatan,'UMUM') as Nama_Jabatan,p.* from pegawai_table p left join jabatan_table j on p.id_jabatan = j.id_jabatan")
    fun getAllTampilanPegawai(): Observable<List<PegawaiModelLengkap>>
}